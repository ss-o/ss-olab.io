---
title: "Real World Onion"
subtitle: "Onion Sites"
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, 
         {src: "/img/sphere.jpg", desc: "Sphere"}, 
         {src: "/img/hexagon.jpg", desc: "Hexagon"}]
tags: ["onion", "tor"]
---

# [Real-World Onion Sites](#index)

This is a list of substantial, commercial-or-social-good mainstream websites which provide onion services.

<!--more-->

- no sites with an "onion-only" presence
- no sites for tech with less than (arbitrary) 10,000 users
- no nudity, exploitation, drugs, copyright infringement or
  sketchy-content sites
- the editor reserves all rights to annotate or drop any or all
  entries as deemed fit
- licensed: cc-by-sa
- author/editor: alec muffett

You can find techical details and the legend/key for symbols in the
[footnotes section](#footnotes), below.

----
# Index

* [Civil Society And Community](#civil-society-and-community)
* [Companies And Services](#companies-and-services)
* [Education](#education)
* [Government](#government)
* [News And Media](#news-and-media)
* [Tech And Software](#tech-and-software)
* [Web And Internet](#web-and-internet)
* [Globaleaks](#globaleaks)
* [Securedrop For Individuals](#securedrop-for-individuals)
* [Securedrop For Organisations](#securedrop-for-organisations)

----
## Civil Society And Community

### [Privacy International](https://privacyintyqcroe.onion/) 
* link: [https://privacyintyqcroe.onion/](https://privacyintyqcroe.onion/)
* plain: `https://privacyintyqcroe.onion/`
* proof: see tls/ssl certificate


### [Riseup Home](http://vww6ybal4bd7szmgncyruucpgfkqahzddi37ktceo3ah7ngmcopnpyyd.onion/) 
* link: [http://vww6ybal4bd7szmgncyruucpgfkqahzddi37ktceo3ah7ngmcopnpyyd.onion/](http://vww6ybal4bd7szmgncyruucpgfkqahzddi37ktceo3ah7ngmcopnpyyd.onion/)
* plain: `http://vww6ybal4bd7szmgncyruucpgfkqahzddi37ktceo3ah7ngmcopnpyyd.onion/`
* proof: [link](https://riseup.net/en/security/network-security/tor#riseups-tor-onion-services)

### [Riseup Onion Index](http://vww6ybal4bd7szmgncyruucpgfkqahzddi37ktceo3ah7ngmcopnpyyd.onion/en/security/network-security/tor#riseups-tor-onion-services) 
*provides shared notepad, file sharing, code hosting, and other services*
* link: [http://vww6ybal4bd7szmgncyruucpgfkqahzddi37ktceo3ah7ngmcopnpyyd.onion/en/security/network-security/tor#riseups-tor-onion-services](http://vww6ybal4bd7szmgncyruucpgfkqahzddi37ktceo3ah7ngmcopnpyyd.onion/en/security/network-security/tor#riseups-tor-onion-services)
* plain: `http://vww6ybal4bd7szmgncyruucpgfkqahzddi37ktceo3ah7ngmcopnpyyd.onion/en/security/network-security/tor#riseups-tor-onion-services`
* proof: [link](https://riseup.net/en/security/network-security/tor#riseups-tor-onion-services)

### [Systemli Home](http://7sk2kov2xwx6cbc32phynrifegg6pklmzs7luwcggtzrnlsolxxuyfyd.onion/en/index.html) 
* link: [http://7sk2kov2xwx6cbc32phynrifegg6pklmzs7luwcggtzrnlsolxxuyfyd.onion/en/index.html](http://7sk2kov2xwx6cbc32phynrifegg6pklmzs7luwcggtzrnlsolxxuyfyd.onion/en/index.html)
* plain: `http://7sk2kov2xwx6cbc32phynrifegg6pklmzs7luwcggtzrnlsolxxuyfyd.onion/en/index.html`
* proof: [link](https://www.systemli.org/en/service/onion.html)

### [Systemli Onion Index](http://7sk2kov2xwx6cbc32phynrifegg6pklmzs7luwcggtzrnlsolxxuyfyd.onion/en/service/onion.html) 
*provides shared notepad, spreadsheet, pastebin, and other services*
* link: [http://7sk2kov2xwx6cbc32phynrifegg6pklmzs7luwcggtzrnlsolxxuyfyd.onion/en/service/onion.html](http://7sk2kov2xwx6cbc32phynrifegg6pklmzs7luwcggtzrnlsolxxuyfyd.onion/en/service/onion.html)
* plain: `http://7sk2kov2xwx6cbc32phynrifegg6pklmzs7luwcggtzrnlsolxxuyfyd.onion/en/service/onion.html`
* proof: [link](https://www.systemli.org/en/service/onion.html)

----
## Companies And Services

### [decoded:Legal](http://dlegal66uj5u2dvcbrev7vv6fjtwnd4moqu7j6jnd42rmbypv3coigyd.onion/) 
*english law firm*
* link: [http://dlegal66uj5u2dvcbrev7vv6fjtwnd4moqu7j6jnd42rmbypv3coigyd.onion/](http://dlegal66uj5u2dvcbrev7vv6fjtwnd4moqu7j6jnd42rmbypv3coigyd.onion/)
* plain: `http://dlegal66uj5u2dvcbrev7vv6fjtwnd4moqu7j6jnd42rmbypv3coigyd.onion/`
* proof: [link](https://solicitors.lawsociety.org.uk/office/593348/decoded-legal-limited)

----
## Education

### [BBC Learning English](http://www.s5rhoqqosmcispfb.onion/learningenglish/)
*includes resources for many languages*
* link: [http://www.s5rhoqqosmcispfb.onion/learningenglish/](http://www.s5rhoqqosmcispfb.onion/learningenglish/)
* plain: `http://www.s5rhoqqosmcispfb.onion/learningenglish/`
* proof: see tls/ssl certificate

### [BBC Learning English: Mandarin](http://www.s5rhoqqosmcispfb.onion/learningenglish/chinese) 
* link: [http://www.s5rhoqqosmcispfb.onion/learningenglish/chinese](http://www.s5rhoqqosmcispfb.onion/learningenglish/chinese)
* plain: `http://www.s5rhoqqosmcispfb.onion/learningenglish/chinese`
* proof: see tls/ssl certificate

----
## Government

### [US Central Intelligence Agency](http://ciadotgov4sjwlzihbbgxnqg3xiyrg7so2r2o3lt5wz5ypk4sxyjstad.onion/index.html) 
* link: [http://ciadotgov4sjwlzihbbgxnqg3xiyrg7so2r2o3lt5wz5ypk4sxyjstad.onion/index.html](http://ciadotgov4sjwlzihbbgxnqg3xiyrg7so2r2o3lt5wz5ypk4sxyjstad.onion/index.html)
* plain: `http://ciadotgov4sjwlzihbbgxnqg3xiyrg7so2r2o3lt5wz5ypk4sxyjstad.onion/index.html`
* proof: [link](https://www.cia.gov/news-information/featured-story-archive/2019-featured-story-archive/latest-layer-an-onion-site.html)

----
## News And Media

### [BBC News](https://www.bbcnewsv2vjtpsuy.onion/) 
* link: [https://www.bbcnewsv2vjtpsuy.onion/](https://www.bbcnewsv2vjtpsuy.onion/)
* plain: `https://www.bbcnewsv2vjtpsuy.onion/`
* proof: [link](https://www.bbc.co.uk/news/technology-50150981)

### [BBC News Arabic | عربى](https://www.s5rhoqqosmcispfb.onion/arabic)
* link: [https://www.s5rhoqqosmcispfb.onion/arabic](https://www.s5rhoqqosmcispfb.onion/arabic)
* plain: `https://www.s5rhoqqosmcispfb.onion/arabic`
* proof: see tls/ssl certificate

### [BBC News Chinese | 中文](https://www.s5rhoqqosmcispfb.onion/ukchina/simp) 
* link: [https://www.s5rhoqqosmcispfb.onion/ukchina/simp](https://www.s5rhoqqosmcispfb.onion/ukchina/simp)
* plain: `https://www.s5rhoqqosmcispfb.onion/ukchina/simp`
* proof: see tls/ssl certificate

### [BBC News Persian | فارسی](https://www.s5rhoqqosmcispfb.onion/persian)
* link: [https://www.s5rhoqqosmcispfb.onion/persian](https://www.s5rhoqqosmcispfb.onion/persian)
* plain: `https://www.s5rhoqqosmcispfb.onion/persian`
* proof: see tls/ssl certificate

### [BBC News Pidgin](https://www.s5rhoqqosmcispfb.onion/pidgin) 
* link: [https://www.s5rhoqqosmcispfb.onion/pidgin](https://www.s5rhoqqosmcispfb.onion/pidgin)
* plain: `https://www.s5rhoqqosmcispfb.onion/pidgin`
* proof: see tls/ssl certificate

### [BBC News Russian | Русская](https://www.s5rhoqqosmcispfb.onion/russian) 
* link: [https://www.s5rhoqqosmcispfb.onion/russian](https://www.s5rhoqqosmcispfb.onion/russian)
* plain: `https://www.s5rhoqqosmcispfb.onion/russian`
* proof: see tls/ssl certificate

### [BBC News Turkish | Türkçe](https://www.s5rhoqqosmcispfb.onion/turkce) 
* link: [https://www.s5rhoqqosmcispfb.onion/turkce](https://www.s5rhoqqosmcispfb.onion/turkce)
* plain: `https://www.s5rhoqqosmcispfb.onion/turkce`
* proof: see tls/ssl certificate

### [BBC News Vietnamese | Tiếng Việt](https://www.s5rhoqqosmcispfb.onion/vietnamese)
* link: [https://www.s5rhoqqosmcispfb.onion/vietnamese](https://www.s5rhoqqosmcispfb.onion/vietnamese)
* plain: `https://www.s5rhoqqosmcispfb.onion/vietnamese`
* proof: see tls/ssl certificate

### [BBC News | In Your Language](https://www.s5rhoqqosmcispfb.onion/ws/languages) 
*language index*
* link: [https://www.s5rhoqqosmcispfb.onion/ws/languages](https://www.s5rhoqqosmcispfb.onion/ws/languages)
* plain: `https://www.s5rhoqqosmcispfb.onion/ws/languages`
* proof: see tls/ssl certificate

### [BuzzFeed News](https://bfnews3u2ox4m4ty.onion/) 
* link: [https://bfnews3u2ox4m4ty.onion/](https://bfnews3u2ox4m4ty.onion/)
* plain: `https://bfnews3u2ox4m4ty.onion/`
* proof: see tls/ssl certificate

### [Deutsche Welle](https://www.dwnewsvdyyiamwnp.onion/) 
*also, see language index in titlebar*
* link: [https://www.dwnewsvdyyiamwnp.onion/](https://www.dwnewsvdyyiamwnp.onion/)
* plain: `https://www.dwnewsvdyyiamwnp.onion/`
* proof: [link](https://www.dw.com/en/deutsche-welle-websites-now-accessible-via-tor-protocol/a-51338328)

### [Deutsche Welle Arabic](https://www.dwnewsvdyyiamwnp.onion/ar/
* link: [https://www.dwnewsvdyyiamwnp.onion/ar/](https://www.dwnewsvdyyiamwnp.onion/ar/)
* plain: `https://www.dwnewsvdyyiamwnp.onion/ar/`
* proof: see tls/ssl certificate

### [Deutsche Welle Chinese](https://www.dwnewsvdyyiamwnp.onion/zh/) 
* link: [https://www.dwnewsvdyyiamwnp.onion/zh/](https://www.dwnewsvdyyiamwnp.onion/zh/)
* plain: `https://www.dwnewsvdyyiamwnp.onion/zh/`
* proof: see tls/ssl certificate

### [Deutsche Welle Persian](https://www.dwnewsvdyyiamwnp.onion/fa-ir/) 
* link: [https://www.dwnewsvdyyiamwnp.onion/fa-ir/](https://www.dwnewsvdyyiamwnp.onion/fa-ir/)
* plain: `https://www.dwnewsvdyyiamwnp.onion/fa-ir/`
* proof: see tls/ssl certificate

### [Deutsche Welle Russian](https://www.dwnewsvdyyiamwnp.onion/ru/)
* link: [https://www.dwnewsvdyyiamwnp.onion/ru/](https://www.dwnewsvdyyiamwnp.onion/ru/)
* plain: `https://www.dwnewsvdyyiamwnp.onion/ru/`
* proof: see tls/ssl certificate

### [Deutsche Welle Turkish](https://www.dwnewsvdyyiamwnp.onion/tr/) 
* link: [https://www.dwnewsvdyyiamwnp.onion/tr/](https://www.dwnewsvdyyiamwnp.onion/tr/)
* plain: `https://www.dwnewsvdyyiamwnp.onion/tr/`
* proof: see tls/ssl certificate

### [ProPublica](https://p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/)
* link: [https://p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/](https://p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/)
* plain: `https://p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/`
* proof: see tls/ssl certificate

### [Reporters Without Borders Helpdesk](https://5qlhz3jow7zhxtpx.onion/) 
* link: [https://5qlhz3jow7zhxtpx.onion/](https://5qlhz3jow7zhxtpx.onion/)
* plain: `https://5qlhz3jow7zhxtpx.onion/`
* proof: [link](https://rsf.org/en/news/reporters-without-borders-launches-digital-help-desk)

### [The New York Times](https://www.nytimes3xbfgragh.onion/) 
* link: [https://www.nytimes3xbfgragh.onion/](https://www.nytimes3xbfgragh.onion/)
* plain: `https://www.nytimes3xbfgragh.onion/`
* proof: [link](https://open.nytimes.com/https-open-nytimes-com-the-new-york-times-as-a-tor-onion-service-e0d0b67b7482)

----
## Tech And Software

### [Ablative Hosting](https://hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion/)
* link: [https://hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion/](https://hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion/)
* plain: `https://hzwjmjimhr7bdmfv2doll4upibt5ojjmpo3pbp5ctwcg37n3hyk7qzid.onion/`
* proof: see tls/ssl certificate

### [DEF CON Groups](http://jrw32khnmfehvdsvwdf34mywoqj5emvxh4mzbkls6jk2cb3thcgz6nid.onion/)
* link: [http://jrw32khnmfehvdsvwdf34mywoqj5emvxh4mzbkls6jk2cb3thcgz6nid.onion/](http://jrw32khnmfehvdsvwdf34mywoqj5emvxh4mzbkls6jk2cb3thcgz6nid.onion/)
* plain: `http://jrw32khnmfehvdsvwdf34mywoqj5emvxh4mzbkls6jk2cb3thcgz6nid.onion/`
* proof: [link](https://www.facebook.com/defcon/posts/i-am-proud-to-announce-the-v3-onion-address-for-def-condefconorg-main-web-siteht/10155438526096656/)

### [DEF CON Home](http://g7ejphhubv5idbbu3hb3wawrs5adw7tkx7yjabnf65xtzztgg4hcsqqd.onion/) 
* link: [http://g7ejphhubv5idbbu3hb3wawrs5adw7tkx7yjabnf65xtzztgg4hcsqqd.onion/](http://g7ejphhubv5idbbu3hb3wawrs5adw7tkx7yjabnf65xtzztgg4hcsqqd.onion/)
* plain: `http://g7ejphhubv5idbbu3hb3wawrs5adw7tkx7yjabnf65xtzztgg4hcsqqd.onion/`
* proof: [link](https://www.facebook.com/defcon/posts/i-am-proud-to-announce-the-v3-onion-address-for-def-condefconorg-main-web-siteht/10155438526096656/)

### [DEF CON Media](http://m6rqq6kocsyugo2laitup5nn32bwm3lh677chuodjfmggczoafzwfcad.onion/) 
* link: [http://m6rqq6kocsyugo2laitup5nn32bwm3lh677chuodjfmggczoafzwfcad.onion/](http://m6rqq6kocsyugo2laitup5nn32bwm3lh677chuodjfmggczoafzwfcad.onion/)
* plain: `http://m6rqq6kocsyugo2laitup5nn32bwm3lh677chuodjfmggczoafzwfcad.onion/`
* proof: [link](https://www.facebook.com/defcon/posts/i-am-proud-to-announce-the-v3-onion-address-for-def-condefconorg-main-web-siteht/10155438526096656/)

### [Debian Home](http://sejnfjrq6szgca7v.onion/)
* link: [http://sejnfjrq6szgca7v.onion/](http://sejnfjrq6szgca7v.onion/)
* plain: `http://sejnfjrq6szgca7v.onion/`
* proof: [link](https://onion.debian.org)

### [Debian Onion Index](http://5nca3wxl33tzlzj5.onion/)
*everything debian*
* link: [http://5nca3wxl33tzlzj5.onion/](http://5nca3wxl33tzlzj5.onion/)
* plain: `http://5nca3wxl33tzlzj5.onion/`
* proof: [link](https://onion.debian.org/)

### [ExpressVPN](http://expressobutiolem.onion/)
* link: [http://expressobutiolem.onion/](http://expressobutiolem.onion/)
* plain: `http://expressobutiolem.onion/`
* proof: [link](https://www.expressvpn.com/blog/expressvpn-launches-tor-onion/)

### [Mailpile](http://clgs64523yi2bkhz.onion/)
* link: [http://clgs64523yi2bkhz.onion/](http://clgs64523yi2bkhz.onion/)
* plain: `http://clgs64523yi2bkhz.onion/`
* proof: [link](https://www.mailpile.is)

### [OnionShare](http://lldan5gahapx5k7iafb3s4ikijc4ni7gx5iywdflkba5y2ezyg6sjgyd.onion/) 
* link: [http://lldan5gahapx5k7iafb3s4ikijc4ni7gx5iywdflkba5y2ezyg6sjgyd.onion/](http://lldan5gahapx5k7iafb3s4ikijc4ni7gx5iywdflkba5y2ezyg6sjgyd.onion/)
* plain: `http://lldan5gahapx5k7iafb3s4ikijc4ni7gx5iywdflkba5y2ezyg6sjgyd.onion/`
* proof: [link](https://onionshare.org/)

### [Qubes OS](http://www.qubesosfasa4zl44o4tws22di6kepyzfeqv3tg4e3ztknltfxqrymdad.onion/)
* link: [http://www.qubesosfasa4zl44o4tws22di6kepyzfeqv3tg4e3ztknltfxqrymdad.onion/](http://www.qubesosfasa4zl44o4tws22di6kepyzfeqv3tg4e3ztknltfxqrymdad.onion/)
* plain: `http://www.qubesosfasa4zl44o4tws22di6kepyzfeqv3tg4e3ztknltfxqrymdad.onion/`
* proof: [link](https://www.qubes-os.org/news/2019/04/17/tor-onion-services-available-again/)

### [Tor Project Home](http://expyuzz4wqqyqhjn.onion/) 
* link: [http://expyuzz4wqqyqhjn.onion/](http://expyuzz4wqqyqhjn.onion/)
* plain: `http://expyuzz4wqqyqhjn.onion/`
* proof: [link](https://onion.torproject.org)

### [Tor Project Onion Index](http://yz7lpwfhhzcdyc5y.onion/)
*everything tor*
* link: [http://yz7lpwfhhzcdyc5y.onion/](http://yz7lpwfhhzcdyc5y.onion/)
* plain: `http://yz7lpwfhhzcdyc5y.onion/`
* proof: [link](https://onion.torproject.org/)

### [Whonix Forums](http://forums.dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/) 
* link: [http://forums.dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/](http://forums.dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/)
* plain: `http://forums.dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/`
* proof: [link](https://www.qubes-os.org/news/2018/01/23/qubes-whonix-next-gen-tor-onion-services/)

### [Whonix Home](http://dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/) 
* link: [http://dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/](http://dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/)
* plain: `http://dds6qkxpwdeubwucdiaord2xgbbeyds25rbsgr73tbfpqpt4a6vjwsyd.onion/`
* proof: [link](https://www.qubes-os.org/news/2018/01/23/qubes-whonix-next-gen-tor-onion-services/)

### [keybase.io](http://keybase5wmilwokqirssclfnsqrjdsi7jdir5wy7y7iu3tanwmtp6oid.onion/) 
* link: [http://keybase5wmilwokqirssclfnsqrjdsi7jdir5wy7y7iu3tanwmtp6oid.onion/](http://keybase5wmilwokqirssclfnsqrjdsi7jdir5wy7y7iu3tanwmtp6oid.onion/)
* plain: `http://keybase5wmilwokqirssclfnsqrjdsi7jdir5wy7y7iu3tanwmtp6oid.onion/`
* proof: [link](https://keybase.io/docs/command_line/tor)

----
## Web And Internet

### [Archive Today (archive.is)](http://archivecaslytosk.onion/)
* link: [http://archivecaslytosk.onion/](http://archivecaslytosk.onion/)
* plain: `http://archivecaslytosk.onion/`
* proof: [link](https://archive.is/)

### [Cloudflare Public DNS 1.1.1.1](https://dns4torpnlfs2ifuz2s2yf3fc7rdmsbhm6rw75euj35pac6ap25zgqad.onion/)
* link: [https://dns4torpnlfs2ifuz2s2yf3fc7rdmsbhm6rw75euj35pac6ap25zgqad.onion/](https://dns4torpnlfs2ifuz2s2yf3fc7rdmsbhm6rw75euj35pac6ap25zgqad.onion/)
* plain: `https://dns4torpnlfs2ifuz2s2yf3fc7rdmsbhm6rw75euj35pac6ap25zgqad.onion/`
* proof: see tls/ssl certificate

### [DuckDuckGo](https://3g2upl4pq6kufc4m.onion/) 
* link: [https://3g2upl4pq6kufc4m.onion/](https://3g2upl4pq6kufc4m.onion/)
* plain: `https://3g2upl4pq6kufc4m.onion/`
* proof: see tls/ssl certificate

### [Facebook](https://www.facebookcorewwwi.onion/)
* link: [https://www.facebookcorewwwi.onion/](https://www.facebookcorewwwi.onion/)
* plain: `https://www.facebookcorewwwi.onion/`
* proof: [link](https://www.facebook.com/notes/protect-the-graph/making-connections-to-facebook-more-secure/1526085754298237/)

### [Facebook Mobile](https://m.facebookcorewwwi.onion/) 
* link: [https://m.facebookcorewwwi.onion/](https://m.facebookcorewwwi.onion/)
* plain: `https://m.facebookcorewwwi.onion/`
* proof: see tls/ssl certificate

### [Internet Archive (archive.org)](http://archivebyd3rzt3ehjpm4c3bjkyxv3hjleiytnvxcn7x32psn2kxcuid.onion/) 
* link: [http://archivebyd3rzt3ehjpm4c3bjkyxv3hjleiytnvxcn7x32psn2kxcuid.onion/](http://archivebyd3rzt3ehjpm4c3bjkyxv3hjleiytnvxcn7x32psn2kxcuid.onion/)
* plain: `http://archivebyd3rzt3ehjpm4c3bjkyxv3hjleiytnvxcn7x32psn2kxcuid.onion/`
* proof: [link](https://archive.org/about/offline-archive/)

### [Mail2Tor](http://mail2tor2zyjdctd.onion/)
* link: [http://mail2tor2zyjdctd.onion/](http://mail2tor2zyjdctd.onion/)
* plain: `http://mail2tor2zyjdctd.onion/`
* proof: [link](http://mail2tor.com)

### [Protonmail](https://protonirockerxow.onion/) 
* link: [https://protonirockerxow.onion/](https://protonirockerxow.onion/)
* plain: `https://protonirockerxow.onion/`
* proof: see tls/ssl certificate

----
## Globaleaks

### [ALAT / Allerta AntiCorruzione](http://fkut2p37apcg6l7f.onion/)
*italian whistleblowing*
* link: [http://fkut2p37apcg6l7f.onion/](http://fkut2p37apcg6l7f.onion/)
* plain: `http://fkut2p37apcg6l7f.onion/`
* proof: [link](https://allertaanticorruzione.transparency.it/servizio-alac/)

### [Afrileaks](http://wcnueib4qrsm544n.onion/) 
* link: [http://wcnueib4qrsm544n.onion/](http://wcnueib4qrsm544n.onion/)
* plain: `http://wcnueib4qrsm544n.onion/`
* proof: [link](https://www.afrileaks.org/)

### [Atlatszo MagyarLeaks](http://ak2uqfavwgmjrvtu.onion/) 
*hungarian leaks*
* link: [http://ak2uqfavwgmjrvtu.onion/](http://ak2uqfavwgmjrvtu.onion/)
* plain: `http://ak2uqfavwgmjrvtu.onion/`
* proof: [link](https://atlatszo.hu/magyarleaks/)

### [Bezkorupce.cz](http://iopx5pchfdldldwp.onion/)
*czech anticorruption reporting site*
* link: [http://iopx5pchfdldldwp.onion/](http://iopx5pchfdldldwp.onion/)
* plain: `http://iopx5pchfdldldwp.onion/`
* proof: [link](https://secure.bezkorupce.cz/)

### [Mexico Leaks](http://kjpkmlafh2ra57wz.onion/)
* link: [http://kjpkmlafh2ra57wz.onion/](http://kjpkmlafh2ra57wz.onion/)
* plain: `http://kjpkmlafh2ra57wz.onion/`
* proof: [link](https://mexicoleaks.mx/)

### [Pistaljka.rs Whistleblowing](http://acabtd4btrxjjrvr.onion/#/) 
* link: [http://acabtd4btrxjjrvr.onion/#/](http://acabtd4btrxjjrvr.onion/#/)
* plain: `http://acabtd4btrxjjrvr.onion/#/`
* proof: [link](https://pistaljka.rs/)

### [Wildleaks](http://ppdz5djzpo3w5k2z.onion/) 
*elephant action league*
* link: [http://ppdz5djzpo3w5k2z.onion/](http://ppdz5djzpo3w5k2z.onion/)
* plain: `http://ppdz5djzpo3w5k2z.onion/`
* proof: [link](https://www.wildleaks.org/the-technology/)

### [XNet Activism](http://ztjn5gcdsqeqzmw4.onion/)
*anticorruption whistleblowing*
* link: [http://ztjn5gcdsqeqzmw4.onion/](http://ztjn5gcdsqeqzmw4.onion/)
* plain: `http://ztjn5gcdsqeqzmw4.onion/`
* proof: [link](https://xnet-x.net/en/xnetleaks/)
----
## Securedrop For Individuals

### [Barton Gellman](http://mqddpn6yt4f5uqei.onion/) 
* link: [http://mqddpn6yt4f5uqei.onion/](http://mqddpn6yt4f5uqei.onion/)
* plain: `http://mqddpn6yt4f5uqei.onion/`
* proof: [link](https://github.com/b4rton/securedrop)

### [Jean-Marc Manach](http://32qfx2skzcifeyg7.onion/)
* link: [http://32qfx2skzcifeyg7.onion/](http://32qfx2skzcifeyg7.onion/)
* plain: `http://32qfx2skzcifeyg7.onion/`
* proof: [link](https://jean-marc.manach.net/securedrop.htm)

----
## Securedrop For Organisations

### [2600: The Hacker Quarterly](http://lxa4rh3xy2s7cvfy.onion/)
* link: [http://lxa4rh3xy2s7cvfy.onion/](http://lxa4rh3xy2s7cvfy.onion/)
* plain: `http://lxa4rh3xy2s7cvfy.onion/`
* proof: [link](https://securedrop.org/directory/2600-hacker-quarterly/)

### [Adresseavisen](http://xpx3m5hcnrkds5wg.onion/) 
* link: [http://xpx3m5hcnrkds5wg.onion/](http://xpx3m5hcnrkds5wg.onion/)
* plain: `http://xpx3m5hcnrkds5wg.onion/`
* proof: [link](https://securedrop.adressa.no/)

### [Aftenposten](http://bocl4xqbak4xvlh4.onion/)
* link: [http://bocl4xqbak4xvlh4.onion/](http://bocl4xqbak4xvlh4.onion/)
* plain: `http://bocl4xqbak4xvlh4.onion/`
* proof: [link](https://www.aftenposten.no/securedrop/)

### [Aftonbladet](https://y27vf7g2ce5g3fnl.onion/) 
* link: [https://y27vf7g2ce5g3fnl.onion/](https://y27vf7g2ce5g3fnl.onion/)
* plain: `https://y27vf7g2ce5g3fnl.onion/`
* proof: see tls/ssl certificate

### [Al-Jazeera](http://aljazeerafo4sau2.onion/)
* link: [http://aljazeerafo4sau2.onion/](http://aljazeerafo4sau2.onion/)
* plain: `http://aljazeerafo4sau2.onion/`
* proof: [link](https://www.aljazeera.net/news/politics/2019/9/27/الجزيرة-نت-معلومات-صور-فيديو-إرسال-مشاركة-تطبيقات)

### [Apache](http://zdf4nikyuswdzbt6.onion/) 
* link: [http://zdf4nikyuswdzbt6.onion/](http://zdf4nikyuswdzbt6.onion/)
* plain: `http://zdf4nikyuswdzbt6.onion/`
* proof: [link](https://www.apache.be/securedrop)

### [Bergens Tidende](http://mxrrw2l3g5dyhgzn.onion/) 
* link: [http://mxrrw2l3g5dyhgzn.onion/](http://mxrrw2l3g5dyhgzn.onion/)
* plain: `http://mxrrw2l3g5dyhgzn.onion/`
* proof: [link](https://www.bt.no/securedrop/)

### [Bloomberg News](http://m4hynbhhctdk27jr.onion/) 
* link: [http://m4hynbhhctdk27jr.onion/](http://m4hynbhhctdk27jr.onion/)
* plain: `http://m4hynbhhctdk27jr.onion/`
* proof: [link](https://www.bloomberg.com/tips)

### [BuzzFeed](http://ndg43ilvrrj465ix.onion/) 
* link: [http://ndg43ilvrrj465ix.onion/](http://ndg43ilvrrj465ix.onion/)
* plain: `http://ndg43ilvrrj465ix.onion/`
* proof: [link](https://tips.buzzfeed.com)

### [CBC / Canadian Broadcasting Corporation](http://gppg43zz5d2yfuom3yfmxnnokn3zj4mekt55onlng3zs653ty4fio6qd.onion/) 
* link: [http://gppg43zz5d2yfuom3yfmxnnokn3zj4mekt55onlng3zs653ty4fio6qd.onion/](http://gppg43zz5d2yfuom3yfmxnnokn3zj4mekt55onlng3zs653ty4fio6qd.onion/)
* plain: `http://gppg43zz5d2yfuom3yfmxnnokn3zj4mekt55onlng3zs653ty4fio6qd.onion/`
* proof: [link](https://www.cbc.ca/securedrop/)

### [Coworker.org](http://no4gurk7efg4abwv.onion/) 
* link: [http://no4gurk7efg4abwv.onion/](http://no4gurk7efg4abwv.onion/)
* plain: `http://no4gurk7efg4abwv.onion/`
* proof: [link](https://home.coworker.org/contact/)

### [Dagbladet](http://mz33367mcdrcdi7s.onion/) 
* link: [http://mz33367mcdrcdi7s.onion/](http://mz33367mcdrcdi7s.onion/)
* plain: `http://mz33367mcdrcdi7s.onion/`
* proof: [link](https://securedrop.dagbladet.no/)

### [Field of Vision](http://2mgwjg7ezpxhx5wb7zktijtnuy6qnobvurtberow52hlij2iw6eo5rad.onion/)
* link: [http://2mgwjg7ezpxhx5wb7zktijtnuy6qnobvurtberow52hlij2iw6eo5rad.onion/](http://2mgwjg7ezpxhx5wb7zktijtnuy6qnobvurtberow52hlij2iw6eo5rad.onion/)
* plain: `http://2mgwjg7ezpxhx5wb7zktijtnuy6qnobvurtberow52hlij2iw6eo5rad.onion/`
* proof: [link](https://fieldofvision.org/securedrop)

### [Financial Times](http://xdm7flvwt3uvsrrd.onion/)
* link: [http://xdm7flvwt3uvsrrd.onion/](http://xdm7flvwt3uvsrrd.onion/)
* plain: `http://xdm7flvwt3uvsrrd.onion/`
* proof: [link](https://www.ft.com/news-tips/)

### [Forbes](http://t5pv5o4t6jyjilp6.onion/) 
* link: [http://t5pv5o4t6jyjilp6.onion/](http://t5pv5o4t6jyjilp6.onion/)
* plain: `http://t5pv5o4t6jyjilp6.onion/`
* proof: [link](https://www.forbes.com/fdc/securedrop.html)

### [Forbidden Stories](http://w7t5f3u4mej6dvpt.onion/)
* link: [http://w7t5f3u4mej6dvpt.onion/](http://w7t5f3u4mej6dvpt.onion/)
* plain: `http://w7t5f3u4mej6dvpt.onion/`
* proof: [link](https://forbiddenstories.org/protect-your-stories/)

### [Gizmodo Media Group](http://gmg7jl25ony5g7ws.onion/)
* link: [http://gmg7jl25ony5g7ws.onion/](http://gmg7jl25ony5g7ws.onion/)
* plain: `http://gmg7jl25ony5g7ws.onion/`
* proof: [link](https://specialprojectsdesk.com/secure-drop)

### [Global Witness](http://37fmdxug33hhyi2g.onion/)
* link: [http://37fmdxug33hhyi2g.onion/](http://37fmdxug33hhyi2g.onion/)
* plain: `http://37fmdxug33hhyi2g.onion/`
* proof: [link](https://www.globalwitness.org/en/securedrop/)

### [Guardian](http://33y6fjyhs3phzfjj.onion/) 
* link: [http://33y6fjyhs3phzfjj.onion/](http://33y6fjyhs3phzfjj.onion/)
* plain: `http://33y6fjyhs3phzfjj.onion/`
* proof: [link](https://www.theguardian.com/securedrop)

### [Heise Investigativ](http://sq4lecqyx4izcpkp.onion/)
* link: [http://sq4lecqyx4izcpkp.onion/](http://sq4lecqyx4izcpkp.onion/)
* plain: `http://sq4lecqyx4izcpkp.onion/`
* proof: [link](https://www.heise.de/investigativ/briefkasten/)

### [Houston Chronicle](http://ibnfpppyydd6mg46.onion/) 
* link: [http://ibnfpppyydd6mg46.onion/](http://ibnfpppyydd6mg46.onion/)
* plain: `http://ibnfpppyydd6mg46.onion/`
* proof: [link](https://newstips.houstonchronicle.com/)

### [HuffPost](http://rbugf2rz5lmjbfun.onion/) 
* link: [http://rbugf2rz5lmjbfun.onion/](http://rbugf2rz5lmjbfun.onion/)
* plain: `http://rbugf2rz5lmjbfun.onion/`
* proof: [link](https://img.huffingtonpost.com/securedrop)

### [ICIJ / International Consortium of Investigative Journalists](http://lzpczap7l3zxu7zv.onion/) 
* link: [http://lzpczap7l3zxu7zv.onion/](http://lzpczap7l3zxu7zv.onion/)
* plain: `http://lzpczap7l3zxu7zv.onion/`
* proof: [link](https://www.icij.org/securedrop)

### [Institute for Quantitative Social Science at Harvard University](http://loshhem4ebuaos3q.onion/) 
* link: [http://loshhem4ebuaos3q.onion/](http://loshhem4ebuaos3q.onion/)
* plain: `http://loshhem4ebuaos3q.onion/`
* proof: [link](https://www.hmdc.harvard.edu/securedrop.html)

### [KUOW Public Radio](http://hcxmf67v3ltykmww.onion/)
* link: [http://hcxmf67v3ltykmww.onion/](http://hcxmf67v3ltykmww.onion/)
* plain: `http://hcxmf67v3ltykmww.onion/`
* proof: [link](https://medium.com/@kuow/how-whistleblowers-can-contact-kuow-3ed089e21d30)

### [Lucy Parsons Labs](http://qn4qfeeslglmwxgb.onion/) 
* link: [http://qn4qfeeslglmwxgb.onion/](http://qn4qfeeslglmwxgb.onion/)
* plain: `http://qn4qfeeslglmwxgb.onion/`
* proof: [link](https://lucyparsonslabs.com/securedrop)
=
### [McClatchy DC](http://zafull3et6muayeh.onion/)
* link: [http://zafull3et6muayeh.onion/](http://zafull3et6muayeh.onion/)
* plain: `http://zafull3et6muayeh.onion/`
* proof: [link](https://www.mcclatchydc.com/customer-service/contact-us/)

### [Meduza](http://xwt2mqq64h63ydp5.onion/) 
* link: [http://xwt2mqq64h63ydp5.onion/](http://xwt2mqq64h63ydp5.onion/)
* plain: `http://xwt2mqq64h63ydp5.onion/`
* proof: [link](https://meduza.io/cards/u-menya-est-vazhnaya-informatsiya-dlya-meduzy-no-ya-boyus-ee-peredavat-kak-sdelat-eto-po-nastoyaschemu-anonimno)

### [NBCNews](http://htikmirac7bzq75o.onion/) 
* link: [http://htikmirac7bzq75o.onion/](http://htikmirac7bzq75o.onion/)
* plain: `http://htikmirac7bzq75o.onion/`
* proof: [link](https://www.nbcnews.com/securedrop)

### [NPR](http://5ha7oig7du2jeyer.onion/)
* link: [http://5ha7oig7du2jeyer.onion/](http://5ha7oig7du2jeyer.onion/)
* plain: `http://5ha7oig7du2jeyer.onion/`
* proof: [link](https://help.npr.org/customer/en/portal/articles/2860413-got-a-confidential-news-tip)

### [NRK](http://nrkvarslekidu2uz.onion/)
* link: [http://nrkvarslekidu2uz.onion/](http://nrkvarslekidu2uz.onion/)
* plain: `http://nrkvarslekidu2uz.onion/`
* proof: [link](https://www.nrk.no/varsle/)

### [New York Times](https://nyttips4bmquxfzw.onion/) 
* link: [https://nyttips4bmquxfzw.onion/](https://nyttips4bmquxfzw.onion/)
* plain: `https://nyttips4bmquxfzw.onion/`
* proof: [link](https://www.nytimes.com/newsgraphics/2016/news-tips/#securedrop)

### [POGO](http://btudph27xfchtl5o.onion)
*project on government oversight*
* link: [http://btudph27xfchtl5o.onion](http://btudph27xfchtl5o.onion)
* plain: `http://btudph27xfchtl5o.onion`
* proof: [link](https://www.pogo.org/report-corruption/)

### [Politico](http://mq2du34rci6arhbd.onion/) 
* link: [http://mq2du34rci6arhbd.onion/](http://mq2du34rci6arhbd.onion/)
* plain: `http://mq2du34rci6arhbd.onion/`
* proof: [link](https://www.politico.com/news-tips/)

### [Public Intelligence](http://arujlhu2zjjhc3bw.onion/)
* link: [http://arujlhu2zjjhc3bw.onion/](http://arujlhu2zjjhc3bw.onion/)
* plain: `http://arujlhu2zjjhc3bw.onion/`
* proof: [link](https://publicintelligence.net/contribute/)

### [RISE Moldova](http://6lhmirnluwmvjw4z.onion/)
* link: [http://6lhmirnluwmvjw4z.onion/](http://6lhmirnluwmvjw4z.onion/)
* plain: `http://6lhmirnluwmvjw4z.onion/`
* proof: [link](https://www.rise.md/leaks/)

### [Reflets.info](http://ehcxwl4f62lkek65.onion/)
* link: [http://ehcxwl4f62lkek65.onion/](http://ehcxwl4f62lkek65.onion/)
* plain: `http://ehcxwl4f62lkek65.onion/`
* proof: [link](https://reflets.info/secure-contact)
=
### [Reuters](http://smb7p276iht3i2fj.onion/)
* link: [http://smb7p276iht3i2fj.onion/](http://smb7p276iht3i2fj.onion/)
* plain: `http://smb7p276iht3i2fj.onion/`
* proof: [link](https://www.reuters.com/investigates/special-report/tips/)

### [San Francisco Chronicle](http://nrwvazcz6figxpg5.onion/) 
* link: [http://nrwvazcz6figxpg5.onion/](http://nrwvazcz6figxpg5.onion/)
* plain: `http://nrwvazcz6figxpg5.onion/`
* proof: [link](https://newstips.sfchronicle.com/)

### [Slate](http://vrsmpjemjfcaimqd.onion) 
* link: [http://vrsmpjemjfcaimqd.onion](http://vrsmpjemjfcaimqd.onion)
* plain: `http://vrsmpjemjfcaimqd.onion`
* proof: [link](https://slate.com/tips)

### [Svenska Dagbladet](http://cnhuql7wj2ga5iv7.onion/) 
* link: [http://cnhuql7wj2ga5iv7.onion/](http://cnhuql7wj2ga5iv7.onion/)
* plain: `http://cnhuql7wj2ga5iv7.onion/`
* proof: [link](https://www.svd.se/securedrop/)

### [Süddeutsche Zeitung](http://rx4g2bilxipcryde.onion/) 
* link: [http://rx4g2bilxipcryde.onion/](http://rx4g2bilxipcryde.onion/)
* plain: `http://rx4g2bilxipcryde.onion/`
* proof: [link](https://www.sueddeutsche.de/projekte/kontakt/en/)

### [The Daily Beast](http://bcwyjiwj25t44it6.onion/) 
* link: [http://bcwyjiwj25t44it6.onion/](http://bcwyjiwj25t44it6.onion/)
* plain: `http://bcwyjiwj25t44it6.onion/`
* proof: [link](https://www.thedailybeast.com/tips)

### [The Globe and Mail (Toronto)](http://sml5wmpuq7ifq2mh.onion/) 
* link: [http://sml5wmpuq7ifq2mh.onion/](http://sml5wmpuq7ifq2mh.onion/)
* plain: `http://sml5wmpuq7ifq2mh.onion/`
* proof: [link](https://sec.theglobeandmail.com/securedrop/)

### [The Intercept](http://intrcept32ncblef.onion/) 
* link: [http://intrcept32ncblef.onion/](http://intrcept32ncblef.onion/)
* plain: `http://intrcept32ncblef.onion/`
* proof: [link](https://theintercept.com/securedrop/)

### [The Washington Post](https://jcw5q6uyjioupxcc.onion/)
* link: [https://jcw5q6uyjioupxcc.onion/](https://jcw5q6uyjioupxcc.onion/)
* plain: `https://jcw5q6uyjioupxcc.onion/`
* proof: [link](https://www.washingtonpost.com/securedrop/)

### [Toronto Crime Stoppers](http://q6jfhwkeyeidwgi2acift4epcxmzaxzxuyy6rezafee4riuniervhnyd.onion) 
* link: [http://q6jfhwkeyeidwgi2acift4epcxmzaxzxuyy6rezafee4riuniervhnyd.onion](http://q6jfhwkeyeidwgi2acift4epcxmzaxzxuyy6rezafee4riuniervhnyd.onion)
* plain: `http://q6jfhwkeyeidwgi2acift4epcxmzaxzxuyy6rezafee4riuniervhnyd.onion`
* proof: [link](https://www.222tips.com/SecureDrop)

### [USA Today](https://usatodayw7vu5egc.onion/) 
* link: [https://usatodayw7vu5egc.onion/](https://usatodayw7vu5egc.onion/)
* plain: `https://usatodayw7vu5egc.onion/`
* proof: [link](https://newstips.usatoday.com/securedrop.html)

### [VG / Verdens Gang](http://r7nd7efjrhvzlf7c.onion/) 
* link: [http://r7nd7efjrhvzlf7c.onion/](http://r7nd7efjrhvzlf7c.onion/)
* plain: `http://r7nd7efjrhvzlf7c.onion/`
* proof: [link](https://www.vg.no/securedrop/)

### [VICE Media](http://e3v3x57ykz25uvij.onion/)
* link: [http://e3v3x57ykz25uvij.onion/](http://e3v3x57ykz25uvij.onion/)
* plain: `http://e3v3x57ykz25uvij.onion/`
* proof: [link](https://news.vice.com/securedrop/)

### [Wall Street Journal](http://z5duvjw7ztnuc6fg.onion/)
* link: [http://z5duvjw7ztnuc6fg.onion/](http://z5duvjw7ztnuc6fg.onion/)
* plain: `http://z5duvjw7ztnuc6fg.onion/`
* proof: [link](https://www.wsj.com/tips)

### [Whistleblower Aid](http://uwd57qermcote3au.onion/) 
* link: [http://uwd57qermcote3au.onion/](http://uwd57qermcote3au.onion/)
* plain: `http://uwd57qermcote3au.onion/`
* proof: [link](https://whistlebloweraid.org/contact/instructions/)

### [Wired](http://k5ri3fdr232d36nb.onion/) 
* link: [http://k5ri3fdr232d36nb.onion/](http://k5ri3fdr232d36nb.onion/)
* plain: `http://k5ri3fdr232d36nb.onion/`
* proof: [link](https://www.wired.com/securedrop/)

### [disclose.ngo](http://eexekfp2ye4h643c.onion) 
* link: [http://eexekfp2ye4h643c.onion](http://eexekfp2ye4h643c.onion)
* plain: `http://eexekfp2ye4h643c.onion`
* proof: [link](https://disclose.ngo/en/article/become-discloser)

### [Žvižgač](http://bhzbgbet5l5p4lb3.onion)
*slovenian whistleblower organisation*
* link: [http://bhzbgbet5l5p4lb3.onion](http://bhzbgbet5l5p4lb3.onion)
* plain: `http://bhzbgbet5l5p4lb3.onion`
* proof: [link](https://zvizgac.si/)

----
## Flaky Sites

These sites have apparently stopped responding.

### [Associated Press](http://3expgpdnrrzezf7r.onion/)
* link: [http://3expgpdnrrzezf7r.onion/](http://3expgpdnrrzezf7r.onion/)
* plain: `http://3expgpdnrrzezf7r.onion/`
* proof: [link](https://www.ap.org/tips/)

### [Business Insider](http://doaxi7t7lkctvq5i.onion/) 
* link: [http://doaxi7t7lkctvq5i.onion/](http://doaxi7t7lkctvq5i.onion/)
* plain: `http://doaxi7t7lkctvq5i.onion/`
* proof: [link](https://www.businessinsider.com/how-to-tip-business-insider-securely-guide-signal-securedrop-2017-6)

### [Fairfax Media Group (SMH et al.)](http://ipfhnseo4hgfw5mg.onion/)
* link: [http://ipfhnseo4hgfw5mg.onion/](http://ipfhnseo4hgfw5mg.onion/)
* plain: `http://ipfhnseo4hgfw5mg.onion/`
* proof: [link](https://securedrop.fairfax.com.au/)

### [Greenpeace New Zealand](http://ll6edwtpfl3zdwoi.onion/) 
* link: [http://ll6edwtpfl3zdwoi.onion/](http://ll6edwtpfl3zdwoi.onion/)
* plain: `http://ll6edwtpfl3zdwoi.onion/`
* proof: [link](https://www.safesource.org.nz)

### [Hardened BSD Onion Index](http://lkiw4tmbudbr43hbyhm636sarn73vuow77czzohdbqdpjuq3vdzvenyd.onion/)
* link: [http://lkiw4tmbudbr43hbyhm636sarn73vuow77czzohdbqdpjuq3vdzvenyd.onion/](http://lkiw4tmbudbr43hbyhm636sarn73vuow77czzohdbqdpjuq3vdzvenyd.onion/)
* plain: `http://lkiw4tmbudbr43hbyhm636sarn73vuow77czzohdbqdpjuq3vdzvenyd.onion/`
* proof: [link](https://hardenedbsd.org/article/shawn-webb/2020-01-30/hardenedbsd-tor-onion-service-v3-nodes)

### [IRPILeaks](http://5r4bjnjug3apqdii.onion/)
*italian investigative reporting project*
* link: [http://5r4bjnjug3apqdii.onion/](http://5r4bjnjug3apqdii.onion/)
* plain: `http://5r4bjnjug3apqdii.onion/`
* proof: [link](https://irpi.eu/en/leaks/how-irpileaks-works/)

### [MormonLeaks / FaithLeaks](http://efeip5ekoqi4upkz.onion/) 
* link: [http://efeip5ekoqi4upkz.onion/](http://efeip5ekoqi4upkz.onion/)
* plain: `http://efeip5ekoqi4upkz.onion/`
* proof: [link](https://mormonleaks.io/)

### [Radio-Canada](http://w5jfqhep2jbypkek.onion/) 
* link: [http://w5jfqhep2jbypkek.onion/](http://w5jfqhep2jbypkek.onion/)
* plain: `http://w5jfqhep2jbypkek.onion/`
* proof: [link](https://sourceanonyme.radio-canada.ca)

### [The Atlantic](http://s6xle2dgrsqcxiwb.onion/) 
* link: [http://s6xle2dgrsqcxiwb.onion/](http://s6xle2dgrsqcxiwb.onion/)
* plain: `http://s6xle2dgrsqcxiwb.onion/`
* proof: [link](https://www.theatlantic.com/tips/)

### [The Telegraph](http://ldbtuktejbkg227d.onion/)
* link: [http://ldbtuktejbkg227d.onion/](http://ldbtuktejbkg227d.onion/)
* plain: `http://ldbtuktejbkg227d.onion/`
* proof: [link](https://www.telegraph.co.uk/news/investigations/contact-us/)

### [The Verge; Racked; Eater](http://2xat73hlwcpwo2zy.onion/)
* link: [http://2xat73hlwcpwo2zy.onion/](http://2xat73hlwcpwo2zy.onion/)
* plain: `http://2xat73hlwcpwo2zy.onion/`
* proof: [link](https://apps.voxmedia.com/verge-tips/)


## Footnotes

- This file (`README.md`) is auto-generated
  - *Do NOT submit changes NOR pull-requests for it*
  - Please submit an `Issue` for consideration / change requests
- If both v2 and v3 addresses are provided for a service, the v3
  address will be preferred / cited
- At the moment where an organisation runs 2+ onion addresses for
  closely related services that do not reflect distinct languages /
  national interests, I am posting a link to an index of their
  onions. Examples: Riseup, Systemli, TorProject, ...
- The master list of Onion SSL EV Certificates may be viewed at
  https://crt.sh/?q=%25.onion


### Codes & Exit Statuses

Mouse-over the icons for details of HTTP codes, curl exit statuses,
and the number of attempts made on each site.

- codes [are from HTTP and are documented elsewhere](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes); RWOS-internal ones include:
  - `901`, `902`, `903` - malformed HTTP response
  - `904` - HTTP status code parse error
  - `910` - connection timeout
- exits [are from Curl and are documented elsewhere](https://curl.haxx.se/libcurl/c/libcurl-errors.html); common ones include:
  - `7` - "curl couldn't connect"
  - `52` - "curl got nothing", received no data from upstream

### TLS Security

Due to the fundamental protocol differences between `HTTP` and
`HTTPS`, it is not wise to consider HTTP-over-Onion to be "as secure
as HTTPS"; web browsers **do** and **must** treat HTTPS requests in
ways that are fundamentally different to HTTP, e.g.:

- with respect to cookie handling, or
- where the trusted connection terminates, or
- how to deal with loading embedded insecure content, or
- whether to permit access to camera and microphone devices (WebRTC)

...and the necessity of broad adherence to web standards would make it
harmful to attempt to optimise just one browser (e.g. Tor Browser) to
elevate HTTP-over-Onion to the same levels of trust as HTTPS-over-TCP,
let alone HTTPS-over-Onion.  Doubtless some browsers will *attempt* to
implement "better-than-default trust and security via HTTP over
onions", but this behaviour will not be **standard**, cannot be
**relied upon** by clients/users, and will therefore be **risky**.

**tl;dr** - HTTP-over-Onion should not be considered as secure as
HTTPS-over-Onion, and attempting to force it thusly will create a
future compatibility mess for the ecosystem of onion-capable browsers.

- :wrench: semi-secure HTTP Onion site, protected by Onion circuits at
  best; will not respect browser secure/HTTPS behaviour
- :closed_lock_with_key: secure HTTPS Onion site, protected by both
  Onion circuits and TLS, will respect browser secure/HTTPS behaviour


