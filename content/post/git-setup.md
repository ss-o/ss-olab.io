### Command line instructions:

> You can upload existing files from your computer using the instructions below.

My git global setup:

```bash
git config --global user.name "Salvydas Lukosius"
git config --global user.email "5275318-sall-lab@users.noreply.gitlab.com"
```

Create a new repository:

```bash
git clone git@gitlab.com:sall-lab/repository.git
cd repository
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

Push an existing folder:

```bash
cd existing_folder
git init
git remote add origin git@gitlab.com:sall-lab/repository.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

Push an existing Git repository:

```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:sall-lab/repository.git
git push -u origin --all
git push -u origin --tags
```

